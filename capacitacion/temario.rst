============
Capacitación
============
-------------------------------------------
Archivo de iniciación en las artes del FOSS
-------------------------------------------

Descripción
===========
Un intento de delimitar cuál es el conocimiento necesario para trabajar en la industria con GNU & Linux; y FOSS en general.


Establecer canales de comunicación
==================================

Email
-----
Servicio de red que permite a los usuarios enviar y recibir mensajes y archivos rápidamente, más información en
http://es.wikipedia.org/wiki/EMail

Es importante saber que hay una etiqueta a seguir, en general, en el mundo de la tecnología:
https://en.wikipedia.org/wiki/Etiquette_in_technology

Google Chat
-----------
Aplicación multiplataforma de mensajería instantánea desarrollada por Google Inc., más información en
https://es.wikipedia.org/wiki/Hangouts

IRC
---
(Internet Relay Chat) Protocolo de comunicación en tiempo real basado en texto, más informacón en
https://es.wikipedia.org/wiki/Internet_Relay_Chat

Registrarse en Freenode
#######################
Red de servidores IRC orientado al software libre, más información en http://es.wikipedia.org/wiki/Freenode

Aprender de la etiqueta de IRC
##############################
Búscala aquí: https://github.com/fizerkhan/irc-etiquette

Matrix
------
Es, al parecer, la evolución natural de IRC (sin serlo directamente). Puedes encontrar más información aquí: https://matrix.org/.

Puedes encontrar los clientes para celular y PC aquí: https://matrix.org/clients/

El cliente que preferimos en EVALinux es Element.

Desde Matrix puedes acceder a canales de IRC y otros protocolos.

Slack
-----
Un cliente de trabajo bastante común. Más información en: https://slack.com/

Teléfonos
---------
Es importante compartir tu contacto con tus colegas; tanto el de tu casa, si tienes, como tu celular. Hay varias ocasiones en las
que es importante estar en contacto y no siempre vemos las notificaicones de la mensajería.


Fundamentales de Linux
======================
* https://www.funtoo.org/Linux_Fundamentals,_Part_1
* https://www.funtoo.org/Linux_Fundamentals,_Part_2
* https://www.funtoo.org/Linux_Fundamentals,_Part_3
* https://www.funtoo.org/Linux_Fundamentals,_Part_4

vim
---
Aprender vim se hace usando el comando: ``vimtutor``.

Como recurso adicional, pudieras probar: https://www.openvim.com/ o buscar algún otro en Google. Hay muchos.

Bash
----
* https://www.funtoo.org/Bash_by_Example,_Part_1
* https://www.funtoo.org/Bash_by_Example,_Part_2
* https://www.funtoo.org/Bash_by_Example,_Part_3

grep
----

awk
---
* https://www.funtoo.org/Awk_by_Example,_Part_1
* https://www.funtoo.org/Awk_by_Example,_Part_2
* https://www.funtoo.org/Awk_by_Example,_Part_3

sed
---
* https://www.funtoo.org/Sed_by_Example,_Part_1
* https://www.funtoo.org/Sed_by_Example,_Part_2
* https://www.funtoo.org/Sed_by_Example,_Part_3

OpenSSH
-------

git
---

find
----

rsync
-----

KVM/Qemu y libvirt
==================

Contenedores
============

Docker
------

Podman
------


Linux (kernel)
==============
* https://www.kernel.org/doc/html/latest/

cgroups v1 y v2
---------------
* man cgroups
* https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v2.html
* https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v1

Syscalls
--------

Procesos
--------

Seguridad
---------

Hardware
--------


Redes
=====

TCP/IP
------

UDP
---

Modelo OSI
----------
Es un modelo para conceptualizar redes muy común y muy usado. Hay que estar bien familiarizados con él.

* https://en.wikipedia.org/wiki/OSI_model


Seguridad
=========

iptables
--------

nftables
--------

SELinux
-------


Servicios web
=============

NginX
-----

PHP
---

Apache HTTPD
------------


Bases de datos
==============

MariaDB/MySQL
-------------

MongoDB
-------

PostgreSQL
----------


Infraestructura
===============

Ansible
-------

MRTG
----

Puppet
------

Salt (saltstack)
----------------

sFlow
-----

Terraform
---------

Zabbix
------


Extras
======

Crystal
-------
Lenguaje de programación compilado y fuertemente tipado. Muy rápido y muy útil. Nuestro favorito.

* https://crystal-lang.org/

HTML, CSS, y Javascript
-----------------------

Python
------

Ruby
----


Documentación
=============

Markdown
--------
Es el markup principalmente usado en la comunidad al rededor de GitHub y GitLab. No es libre y no lo preferimos, pero lo usamos de
todos modos cuando el proyecto lo requiere.

* https://www.markdownguide.org/

reStructured Text
-----------------
Es el markup preferido por ser software libre y ser bastante convertible a varios formatos; incluyendo PDF, html, man y demás.

* https://docutils.sourceforge.io/rst.html

Recursos válidos
----------------
#. ArchLinux Wiki
#. CentOS Documentation
#. Fedora Documentation
#. GNU
#. Gentoo Wiki
#. Página oficial del proyecto
#. Red Hat Documentation
#. Wikipedia

