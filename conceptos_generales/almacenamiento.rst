=====
BtrFS
=====


* Es un sistema de archivos B-tree.
* Inicialmente diseñado por **Oracle Corporation**.
* Después se integraron otras compañías: **Facebook**, **Fujitsu**, **Fusion-IO**, **Intel**, **Linux Foundation**, **Netgear**, **Red Hat**, **STRATO AG** y **SUSE**.
* Su desarrollo comenzó con el 2007.
* Fue lanzado en el 2009.
* Fue marcado como *estable* hasta el 2013.
* Principal autor: Chris Mason.
* Tiene naturaleza *Copy-On-Write*
* De uso exclusivo en Linux.


===
LVM
===


* Es un mapeador de dispositivos.
* Autor: Heinz Mauelshagen.
* Crear particiones y modificar su tamaño posterior a la instalación.
* Crear *Logical volumes* a partir de uno o varios discos duros como si fuera virtualmente uno sólo.


===
XFS
===


* Creador: Silicon Graphics.
* Es un sistema de archivos de 64 bits.
* Salió en el año 1994.
* Tamaño máximo de archivo: 8 exabytes.
* Tamaño máximo de volumen: 16 exabytes.


====
Ext4
====


* Nombre: Fourth Extended File System.
* Es la evolución de **ext3**.
* Creado por: Mingming Cao, Andreas Dilger, etc.
* Es un sistema de archivos
