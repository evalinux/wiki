# general
options {
    listen-on port 53 { localnets; localhost; };
    listen-on-v6 port 53 { localnets; localhost; };
    directory "/var/named";
    dump-file "/var/named/data/cache_dump.db";
    statistics-file "/var/named/data/named_stats.txt";
    memstatistics-file "/var/named/data/named_mem_stats.txt";

    allow-query { localnets; localhost; };
    recursion yes;
    dnssec-enable yes;
    dnssec-validation yes;
    bindkeys-file "/etc/named.iscdlv.key";
    managed-keys-directory "/var/named/dynamic";

    pid-file "/run/named/named.pid";
    session-keyfile "/run/named/session.key";
};

controls {
    inet 127.0.0.1 port 953
    allow { 127.0.0.1; } keys { "EvaKey"; };
};

logging {
    channel default_debug {
        file "data/named.run";
        severity dynamic;
    };
};

# zones
zone "." IN {
    type hint;
    file "named.ca";
};

zone "example.tld" IN {
    type master;
    file "masters/example.tld.db";
};

include "/etc/named.rfc1912.zones";
include "/etc/rndc.key";

